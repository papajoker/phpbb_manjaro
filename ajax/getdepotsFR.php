<style>
    ul.mirrors { margin:0.8em 0 0.8em 2em; padding:0; list-style-type: none; }
    ul.mirrors span { padding:0 4px; font-size:1.3em; }
</style>
<?php 

/**
    retourne état des serveurs fr
    http://repo.manjaro.org/
*/
define('url', 'http://repo.manjaro.org/mirrors.json');
define('status', 'http://repo.manjaro.org/status.json');

$content = json_decode ( file_get_contents(status) );


function showContry($content, $country = 'France') {
    $ok=' style="color:#393"';
    $ko=' style="color:#d00"';

    echo '<ul class="mirrors">';
    foreach ($content as $item) {
        if ( $item->country == $country ){
            echo "<li>";
            echo '<span title="stable"'.($item->branches[0]==1?$ok:$ko).">●</span> ";
            echo '<span title="testing"'.($item->branches[1]==1?$ok:$ko).">●</span> ";
            echo '<span title="instable"'.($item->branches[2]==1?$ok:$ko).">●</span> ";
            echo "<em title=\"Dernier accès il y a ".$item->last_sync." heures\">" . substr(strstr($item->url,'//'), 2) . "</em>";
            echo "</li>";
        }
    }
    echo '</ul>';
}

showContry($content, 'France');
if (isset($_GET["c"]) && $_GET["c"]=='be') showContry($content, 'Belgium');

//var_dump($content);

?>
