<?php
/*
retourne annonce de l'inter
https://forum.manjaro.org/c/announcements/stable-updates.rss

curl -I http://manjaro.local/ajax/getMajInter.php
*/ 
header("Content-type: text/plain");
//header("Content-type: text/html");
$url='https://forum.manjaro.org/c/announcements/stable-updates.rss';
//$url='stable-updates.rss'; // pour tests

function getActu($feed_url) {
    $content = file_get_contents($feed_url);
    $x = new SimpleXmlElement( $content,LIBXML_NOCDATA,false ); 

    foreach($x->channel->item as $entry) {
        $content=str_replace( '<hr>','',$entry->description->__tostring() );
        $content=str_replace( ['<blockquote>','</blockquote>'],'',$content );
        $content=str_replace( ['<p>','</p>'],'',$content );
        $content=str_replace( '<br>','',$content );

        $content= new SimpleXmlElement( '<xml>'.$content.'</xml>' ); 
        
        
echo $entry->title."
      
Bonjour à toutes et à tous  :bjr: 

Une mise à jour est [url=".$entry->link."]annoncée[/url]


[i] le message ... [/i]


N'hésitez pas à nous donner vos commentaires sur les modifications apportées aux dépôts.


-------------------------------------------------------------------------------------
Mise à jour possible par pamac, octopi [size=150][color=#000000]ou par pacman (de préférence)[/color][/size] :
[code]sudo pacman-mirrors -g && sudo pacman -Syu && yaourt -Syua[/code]
-------------------------------------------------------------------------------------


[b][u]Noyaux pris en charge :[/u][/b]
[list]";
    foreach($content->xpath('//ul/li') as $li) {
        $color='';
        if (substr($li,0,5)=='linux') {
            echo "[*]$li".(strpos($li,'EOL')>0?' [color=#FF0000]En fin de vie[/color]':'')."\n";
        }
    }
// $(grep "<li>.*linux" "$tmp_file" | sed -e "s|<li>|[*]|g" -e "s|</li>||" -e "s|\[EOL\]|[EOL] [color=#FF0000]En fin de vie[/color]|")
echo "[/list]

[b][u]Les changements en détail :[/u][/b]
[list]";
    foreach($content->xpath('//ul/li') as $li) {
        $color='';
        if (substr($li,0,6)=='stable') {
            $li = str_replace('new and','nouveaux et',$li);
            $li = str_replace('removed package(s)','paquets retirés',$li);
            echo "[*] ".$li."\n";
        }
    }
echo "[/list]";

echo 
"[b][u]État des miroirs en temps réel :[/u][/b]
[mirrors][/mirrors]


-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
[size=150][color=#BF0000]ERREUR : conflit de fichiers[/color][/size]
Si lors de la mise à jour vous avez le type d'erreur comme ci-dessous (tlp est un exemple) :
[code]erreur : la validation de la transaction a échoué (conflit de fichiers)
tlp : /etc/acpi/events/thinkpad-radiosw est déjà présent dans le système de fichiers
Des erreurs se sont produites, aucun paquet n’a été mis à jour.[/code]
il convient de passer la commande suivante en l'adaptant à votre cas particulier :
[code]sudo pacman -S --force tlp[/code]
Ensuite, vous pouvez faire votre mise à jour générale sans problème.
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
[size=150][color=#BF0000]ERREUR de clés[/color][/size]
Si lors de la mise à jour vous avez des problèmes de clés, essayez de passer les commandes suivantes :
[code]sudo pacman-mirrors -g
sudo pacman -Syy
sudo pacman -S archlinux-keyring manjaro-keyring
sudo pacman-key --init
sudo pacman-key --populate archlinux manjaro
sudo pacman-key --refresh-keys[/code]
Ensuite, vous devriez pouvoir faire votre mise à jour générale sans problème.
-------------------------------------------------------------------------------------
";



        break;
    }
}

getActu($url);
