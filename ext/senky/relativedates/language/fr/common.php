<?php
/**
*
* Relative dates extension for the phpBB Forum Software package.
*
* @copyright (c) 2016 Jakub Senko <jakubsenko@gmail.com>
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'RELATIVE_DATES'	=> 'Voir dates relatives',

	'R_AGO'			=> 'il y a %s',
	'R_FROM_NOW'	=> '%s depuis maintenant',
	'R_AFTER'		=> '%s après',
	'R_BEFORE'		=> '%s avant',
	'R_YEAR'		=> array(
		1	=> '1 an',
		2	=> '%d ans',
	),
	'R_MONTH'		=> array(
		1	=> '1 mois',
		2	=> '%d mois',
	),
	'R_WEEK'		=> array(
		1	=> '1 semaine',
		2	=> '%d semaines',
	),
	'R_DAY'			=> array(
		1	=> '1 jour',
		2	=> '%d jours',
	),
	'R_HOUR'		=> array(
		1	=> '1 heure',
		2	=> '%d heures',
	),
	'R_MINUTE'		=> array(
		1	=> '1 minute',
		2	=> '%d minutes',
	),
	'R_SECOND'		=> array(
		1	=> '1 seconde',
		2	=> '%d secondes',
	),

	'R_JANUARY'		=> 'Janvier',
	'R_FEBRUARY'	=> 'Février',
	'R_MARCH'		=> 'Mars',
	'R_APRIL'		=> 'Avril',
	'R_MAY'			=> 'Mai',
	'R_JUNE'		=> 'Juin',
	'R_JULY'		=> 'Juillet',
	'R_AUGUST'		=> 'Aout',
	'R_SEPTEMBER'	=> 'September',
	'R_OCTOBER'		=> 'Octobre',
	'R_NOVEMBER'	=> 'Novembre',
	'R_DECEMBER'	=> 'Décembre',

	'R_MONDAY'		=> 'Lundi',
	'R_TUESDAY'		=> 'Mardi',
	'R_WEDNESDAY'	=> 'Mercredi',
	'R_THURSDAY'	=> 'Jeudi',
	'R_FRIDAY'		=> 'Vendredi',
	'R_SATURDAY'	=> 'Samedi',
	'R_SUNDAY'		=> 'Dimanche',
));
