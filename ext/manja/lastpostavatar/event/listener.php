<?php

/**
*
* @package First Last Post Avatar
* @copyright papajoke 2017
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace manja\lastpostavatar\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\path_helper */
	protected $path_helper;

	/** @var \phpbb\request\request */
	protected $request;

	/** @var \phpbb\template\template */
	protected $template;
	
	/** @var \phpbb\auth\auth */
	protected $auth;

	protected $db;	

	public function __construct(
        \phpbb\user $user, 
        \phpbb\template\template $template, 
        \phpbb\request\request $request, 
        \phpbb\auth\auth $auth,
		\phpbb\db\driver\driver_interface $db )
	{
		$this->user = $user;
		$this->template = $template;
		$this->request = $request;
		$this->auth = $auth;
		$this->db = $db;
	}


	static public function getSubscribedEvents()
	{
		return array(
			/* Forumlist last avatar */
			'core.display_forums_modify_sql'			=> 'display_forums_modify_sql',
			'core.display_forums_modify_forum_rows'		=> 'forums_modify_forum_rows',
			'core.display_forums_modify_template_vars'	=> 'display_forums_modify_template_vars',
			/* Viewforum last avatar */
			'core.viewforum_get_topic_data'				=> 'add_user_viewforum_sql',
			'core.viewforum_modify_topicrow'			=> 'viewforum_last_post_avatar',
			/* Search last avatar */
			'core.search_get_topic_data'				=> 'add_user_search_sql',
			/* Connect Recent topics By PayBas */
			'paybas.recenttopics.sql_pull_topics_data'	=> 'add_user_rct_sql',
			/* Formulaire conf profil perso */
			'core.ucp_prefs_personal_data'				=> 'ucp_prefs_get_data',
			'core.ucp_prefs_personal_update_data'		=> 'ucp_prefs_set_data',
			/* modifier un post */
			'core.modify_posting_parameters'			=> 'modify_message_text',
			/* contenu d'un sujet/topic */
			'core.viewtopic_highlight_modify'			=> 'view_topic',

			'core.page_header_after'					=> 'assign_common_template_variables',
		);
	}

	public function main($event)
	{
		$this->template->assign_vars(array(
			'S_AVATARS_ROND' => !empty($this->user->data['user_avatar_rond']) ? true : false,
			'S_COLOR_STYLE' => ''
		));
	}
		

    /** Get user's option and display it in UCP Prefs View page */
	public function ucp_prefs_get_data($event)
	{
        $color_style = $this->request->variable('color_style', $this->user->data['user_style_color']);
        // test if file color_proflat.XXXXXX.css exist
        if (! file_exists('./styles/'.$this->user->style['style_path'].'/theme/color_proflat.'.$color_style.'.css')) {
            $color_style='';
        }

		$event['data'] = array_merge($event['data'], array(
			'user_avatar_rond'	=> $this->request->variable('user_avatar_rond', (int) $this->user->data['user_avatar_rond']),
			'color_style'	=> $color_style,
		));
		// Output the data vars to the template (except on form submit)
		if (!$event['submit'])
		{
			$this->template->assign_vars(array(
				'S_AVATARS_ROND'	=> $event['data']['user_avatar_rond'],
				'S_COLOR_STYLE'	    => $color_style,
			));
		}
	}

	/** Add user's option state into the sql_array */
	public function ucp_prefs_set_data($event)
	{
		$event['sql_ary'] = array_merge($event['sql_ary'], array(
			'user_avatar_rond' => $event['data']['user_avatar_rond'],
			'user_style_color' => $event['data']['color_style'],
		));
	}	
	
	/** Add new colum in sql request */
	public function display_forums_modify_sql($event)
	{
		$sql_ary = $event['sql_ary'];
		$sql_ary['LEFT_JOIN'][] = array(
			'FROM' => array(USERS_TABLE => 'u'),
			'ON' => "u.user_id = f.forum_last_poster_id AND forum_type != " . FORUM_CAT
		);
		$sql_ary['SELECT'] .= ', u.user_avatar as last_post_avatar';
		$event['sql_ary'] = $sql_ary;
	}

	/** stoquer info dans tableau forum_row */
	public function forums_modify_forum_rows($event)
	{
        $forum_rows = $event['forum_rows'];
		if ($event['row']['forum_last_post_time'] == $forum_rows[$event['parent_id']]['forum_last_post_time'])
		{
			$forum_rows[$event['parent_id']]['last_post_avatar'] =$event['row']['last_post_avatar'];
			$event['forum_rows'] = $forum_rows;
		}
	}
	
	/** injecte dans le template */
	public function display_forums_modify_template_vars($event)
	{
		$forum_row['LAST_POST_AVATAR'] = isset($event['row']['last_post_avatar'])?$event['row']['last_post_avatar'] :null;
		//var_dump($event['row']);
		$forum_row['LAST_POST_TIME_SHORT'] = date('d/m à h:s',$event['row']['forum_last_post_time']);
		$event['forum_row'] += $forum_row;
	}
	

	/** viewforum_body: Data request viewforum */
	public function add_user_viewforum_sql($event)
	{
		$sql_array = $event['sql_array'];
		$sql_array['LEFT_JOIN'][] = array(
			'FROM' => array(USERS_TABLE => 'u'),	'ON' => "u.user_id = t.topic_last_poster_id"
		);
		$sql_array['LEFT_JOIN'][] = array(
			'FROM' => array(USERS_TABLE => 'uf'),	'ON' => "uf.user_id = t.topic_poster"
		);				
		$sql_array['SELECT'] .= ', u.user_avatar as last_post_avatar, uf.user_avatar as topic_first_poster_avatar, uf.username_clean as ucf';
		$event['sql_array'] = $sql_array;
	}
	
	/* viewforum_body: User avatar Last post in viewforum */
	public function viewforum_last_post_avatar($event)
	{
        $row = $event['row'];
		$topic_row = $event['topic_row'];
/*
TODO: recup première lettre du pseudo et retourner /images/avatars/gallery/"A".png
*/		
		$ch = strtoupper( substr($event['row']['topic_last_poster_name'],0,1) );
		if (ctype_alnum($ch)) $topic_row['LAST_POST_DEFAULT_AVATAR'] = $ch;
		$ch = isset($event['row']['ucf'])?strtoupper( substr($event['row']['ucf'],0,1) ) :null;
		if (ctype_alnum($ch)) $topic_row['FIRST_POST_DEFAULT_AVATAR'] = $ch;
        $topic_row['LAST_POST_AVATAR'] = isset($event['row']['last_post_avatar'])?$event['row']['last_post_avatar'] :null;
		$topic_row['FIRST_POST_AVATAR'] = isset($event['row']['topic_first_poster_avatar'])?$event['row']['topic_first_poster_avatar'] :null;
        $topic_row['LAST_POST_TIME_SHORT'] = date('d/m à h:s',$event['row']['topic_last_post_time']);
        $event['topic_row'] = $topic_row;
		//var_dump( $event['row']);
	}	

	/** Data request reccent topics */
		/*
		topic_poster = USERS_TABLE.user_id
		QUERY #18
	*/
	public function add_user_search_sql($event)
	{
		$event['sql_from'] .= ' LEFT JOIN ' . USERS_TABLE . ' us ON (us.user_id = t.topic_last_post_id)
		 LEFT JOIN ' . USERS_TABLE . ' usf ON (usf.user_id = t.topic_poster)';
		$event['sql_select'] .= ', us.user_avatar as last_post_avatar, usf.user_avatar as topic_first_poster_avatar';
	}

	/** Data request reccent topics */
	public function add_user_rct_sql($event)
	{
		$sql_array = $event['sql_array'];
		$sql_array['LEFT_JOIN'][] = array(
			'FROM' => array(USERS_TABLE => 'u'),
			'ON' => "u.user_id = t.topic_last_poster_id"
		);
		$sql_array['SELECT'] .= ', u.user_avatar as last_post_avatar';
		$event['sql_array'] = $sql_array;
	}

	/** hook debut du topic */
	function view_topic($event) {
		$sql='SELECT icons_alt FROM '.ICONS_TABLE.' WHERE icons_id='.(int)$event['topic_data']['icon_id'].' LIMIT 1';
		$result = $this->db->sql_query($sql);
		$is_resolved = $this->db->sql_fetchfield('icons_alt') == 'résolu';
		$this->db->sql_freeresult($result);
		$this->template->assign_vars([
			'TOPIC_RESOLVED' => $is_resolved,
			'TOPIC_FIRST_POST' =>$event['topic_data']['topic_first_post_id']
		]);
	}

	/** modifier un post existant */
	function modify_message_text($event){
		if ($event['mode']<>'validate') return;

		if ($this->user->data['user_id']<2 || ! $this->auth->acl_gets('m_edit', $event['forum_id'])) {
			trigger_error('NOT_AUTHORISED');
		};

		$sql= 'SELECT poster_id FROM '. POSTS_TABLE .' WHERE post_id='.(int)$event['post_id'].' LIMIT 1';
		echo $sql;
		$result = $this->db->sql_query($sql);
		$poster = (int) $this->db->sql_fetchfield('poster_id');
		$this->db->sql_freeresult($result);
		if ( $poster != $this->user->data['user_id'] || ! $this->auth->acl_gets('a_', 'm_') ) {
			trigger_error('NOT_AUTHORISED');
		}

		/*
		add_form_key('resolu');
		echo '<br />'.$token = $this->request->variable('form_token', '');
		if (!check_form_key('resolu')) {
			//trigger_error('FORM_INVALID');
		}
		*/
		
		// cherche quelle icone est "resolu"
		$icon=0;
		$sql= 'SELECT icons_id FROM '. ICONS_TABLE .' WHERE icons_alt="résolu" LIMIT 1';
		$result = $this->db->sql_query($sql);
		$icon = (int) $this->db->sql_fetchfield('icons_id');
		if ($icon>0) {
			$sql= 'UPDATE '. POSTS_TABLE .' SET icon_id=IF(icon_id!='.$icon.','.$icon. ',0) WHERE post_id='.$event['post_id'];
			$result = $this->db->sql_query($sql);
			$sql= 'UPDATE '. TOPICS_TABLE .' SET icon_id=IF(icon_id!='.$icon.','.$icon. ',0) WHERE topic_id='.$event['topic_id'];
			$result = $this->db->sql_query($sql);
		}
		$this->db->sql_freeresult($result);
		
		header('Location: '.str_replace('&amp;','&',$this->user->referer));
		exit;
	}

/*
	SELECT count(poster_id) as nb, poster_id, phpbb_users.username_clean  FROM phpbb_posts 
	LEFT JOIN phpbb_users ON phpbb_users.user_id=phpbb_posts.poster_id
	WHERE 
		from_unixtime(post_time) > DATE_SUB(CURDATE(), interval 130 DAY)
		AND group_id<4 AND poster_id>0
	GROUP BY poster_id
	ORDER BY nb DESC
	LIMIT 8
*/

	/**
	 * Assign common template variables
	 *
	 * @param object $event The event object
	 * @return null
	 * @access public
	 */
	public function assign_common_template_variables($event)
	{
		$this->template->assign_vars(array(
			'S_AVATARS_ROND'	=> isset($this->user->data['user_avatar_rond'])? $this->user->data['user_avatar_rond'] :0 ,
			/* information est un modo ou admin */
			'ADMIN_MOD_VAR' => $this->auth->acl_gets('a_', 'm_'),
			'S_COLOR_STYLE'	=> isset($this->user->data['user_style_color'])? $this->user->data['user_style_color'] :'' ,
			/* viewtopic_body */
			'USER_ID_VISIT' => isset($this->user->data['user_id'])? $this->user->data['user_id'] : 0 ,
		));
	}

}
