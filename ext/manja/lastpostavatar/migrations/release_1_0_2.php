<?php

namespace manja\lastpostavatar\migrations;

class release_1_0_2 extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return $this->db_tools->sql_column_exists($this->table_prefix.'users', 'user_avatar_rond');
	}
	public function update_schema()
	{
		return array(
			'add_columns'	=> array(
				$this->table_prefix.'users'	=> array(
					'user_avatar_rond'	=> array('BOOL', 1),
					'user_style_color'	=> array('VCHAR:12', ''),
				),
			),
		);
	}
	public function revert_schema()
	{
		return array(
			'drop_columns'	=> array(
				$this->table_prefix.'users'	=> array(
					'user_avatar_rond',
					'user_style_color',
				),
			),
		);
	}
} 
