<?php

/**
 *
 * @package phpBB Extension - mChat
 * @copyright (c) 2016 dmzx - http://www.dmzx-web.net
 * @copyright (c) 2016 kasimi - https://kasimi.net
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace manja\lastpostavatar\ucp;

class ucp_avatar_info
{
	function module()
	{
		global $config;

		return array(
			'filename'	=> '\manja\lastpostavatar\ucp\ucp_avatar_module',
			'title'		=> 'UCP_AVATAR_CONFIG',
			'version'	=> $config['avatar_version'],
			'modes'		=> array(
				'configuration'	=> array(
					'title' => 'UCP_AVATAR_CONFIG',
					'cat'	=> array('UCP_AVATAR_CONFIG')
				),
			),
		);
	}
}